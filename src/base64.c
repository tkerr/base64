/******************************************************************************
 * base64.c
 * Adapted from base64.c developed for FreeBSD wpa_supplicant and hostapd
 * Copyright (c) 2005-2011, Jouni Malinen <j@w1.fi>
 *
 * See https://web.mit.edu/freebsd/head/contrib/wpa/src/utils/base64.c
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file
 * @brief Base 64 (RFC1341) encode and decode function implementation.  
 * All memory management is performed by the caller.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <string.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "base64.h"

 
/******************************************************************************
 * Forward references.
 ******************************************************************************/

 
/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
 
 
/******************************************************************************
 * Local objects and data.
 ******************************************************************************/
static const unsigned char base64_table[65] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
static unsigned char dtable[256];
    

/******************************************************************************
 * Public functions and methods.
 ******************************************************************************/
 
/**
 * @brief Initialize the Base64 decoder.
 *
 * Call once at startup before calling base64_encode() or base64_decode().
 */
void base64_init(void)
{
    size_t i;
    
    memset(dtable, 0x80, 256);
    
	for (i = 0; i < sizeof(base64_table) - 1; i++)
		dtable[base64_table[i]] = (unsigned char) i;
	dtable['='] = 0;
}

// ----------------------------------------------------------------------------

/**
 * @brief Encode a binary buffer into Base64 format
 * @param src: Data to be encoded
 * @param len: Length of the data to be encoded
 * @param dst: Encoded data buffer
 * @return Length of encoded data
 *
 * Caller is responsible for providing a destination data buffer large enough 
 * to hold the encoded data. Encoded buffer size is computed as follows:
 *   - Source data size * 4/3  (Base64 converts 3 binary bytes into 4 text bytes)
 *   - A null terminator is added to the encoded data buffer to make it easier 
 *     to use as a C string.  The null terminator is not included in the 
 *     returned length value.
 */
size_t base64_encode(const unsigned char* src, size_t len, unsigned char* dst)
{
    unsigned char* pos;
	const unsigned char* end;
    const unsigned char* in;
	size_t olen;
    
    end = src + len;
	in = src;
	pos = dst;
    
	while (end - in >= 3) {
		*pos++ = base64_table[in[0] >> 2];
		*pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
		*pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
		*pos++ = base64_table[in[2] & 0x3f];
		in += 3;
	}

	if (end - in) {
		*pos++ = base64_table[in[0] >> 2];
		if (end - in == 1) {
			*pos++ = base64_table[(in[0] & 0x03) << 4];
			*pos++ = '=';
		} else {
			*pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
			*pos++ = base64_table[(in[1] & 0x0f) << 2];
		}
		*pos++ = '=';
	}
   
	*pos = '\0';
    olen = pos - dst;
	return olen;
}

// ----------------------------------------------------------------------------

/**
 * @brief Decode a Base64 data buffer into binary format
 * @param src: Data to be decoded
 * @param len: Length of the data to be decoded
 * @param dst: Decoded data buffer
 * @return Length of decoded data
 *
 * Caller is responsible for providing a destination data buffer large enough 
 * to hold the decoded data.
 */
size_t base64_decode(const unsigned char* src, size_t len, unsigned char* dst)
{
    unsigned char block[4];
    unsigned char tmp;
    unsigned char* pos;
	size_t i;
    size_t count;
    size_t olen;
	int pad = 0;

	count = 0;
	for (i = 0; i < len; i++) {
		if (dtable[src[i]] != 0x80)
			count++;
	}

	if (count == 0 || count % 4)
		return 0;

	pos = dst;

	count = 0;
	for (i = 0; i < len; i++) {
		tmp = dtable[src[i]];
		if (tmp == 0x80)
			continue;

		if (src[i] == '=')
			pad++;
		block[count] = tmp;
		count++;
		if (count == 4) {
			*pos++ = (block[0] << 2) | (block[1] >> 4);
			*pos++ = (block[1] << 4) | (block[2] >> 2);
			*pos++ = (block[2] << 6) | block[3];
			count = 0;
			if (pad) {
				if (pad == 1)
					pos--;
				else if (pad == 2)
					pos -= 2;
				else {
					/* Invalid padding */
					return 0;
				}
				break;
			}
		}
	}

	olen = pos - dst;
	return olen;
}

// ----------------------------------------------------------------------------

/******************************************************************************
 * Private functions and methods.
 ******************************************************************************/

 
// End of file.

