/******************************************************************************
 * base64.h
 * Adapted from base64.c developed for FreeBSD wpa_supplicant and hostapd
 * Copyright (c) 2005-2011, Jouni Malinen <j@w1.fi>
 *
 * See https://web.mit.edu/freebsd/head/contrib/wpa/src/utils/base64.c
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file
 * @brief Base 64 (RFC1341) encode and decode function definitions.
 */
#ifndef _BASE64_H
#define _BASE64_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stddef.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/


/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
#ifdef __cplusplus
    extern "C" {
#endif

void   base64_init(void);
size_t base64_encode(const unsigned char* src, size_t len, unsigned char* dst);
size_t base64_decode(const unsigned char* src, size_t len, unsigned char* dst);


#ifdef __cplusplus
    }
#endif

#endif // _BASE64_H
